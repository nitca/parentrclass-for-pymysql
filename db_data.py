"""
Database access and tables for mysql
"""

LOGIN = 'login'
PASSWORD = 'password'
DB = 'database'
HOST = 'localhost'

USERS_TABLE = 'users'

USERS_TABLE_DATA = \
    "user_id int(11) unsigned NOT NULL DEFAULT 0 UNIQUE, "\
    "status tinyint NOT NULL DEFAULT 0, "\
    "perms tinyint NOT NULL DEFAULT 0, " \
    "points int unsigned NOT NULL DEFAULT 0"
